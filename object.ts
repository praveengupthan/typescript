/*
What is Object : 

1. Object is the primary concept of OOP (Object Oriented programming)
2. "Object " represents a physical item, that represents a person or a thing
3. Object is collection of properties (details) and methods (manipulations)
4. for example, a student is an "object"
5. We can create any number of objects inside the program 

What is Property

1. Propeties are the details about the object
2. Properteis are used to store a value
3. For Example , studentname="Scott" is property in the "student object"
4. Properties are stores inside the object
5. The value of property can be changed any no.of times

Waht is Method

1. Methods are the opertions / tasks of the object, which manipulates/calculates the data and do some process
2. Method is function in the object, In other words a function which is stored inside the object is called as "method"
3. Methods are stored inside the object

*/

let student = 
{
    Id:1, 
    Name:"Praveen Guptha Nandipatyi",
    Location:"Hyderabad",
    marks:80,

    getResults(){
        if(this.marks>=30){
            return "You have passed"
        }
        return "You are failed"
    }
};

console.log(student);
console.log(student.Id);
console.log(student.getResults());


