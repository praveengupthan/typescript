/*
Waht is Class?

"Class"represents a model of the object, which defines list of properties and methods of the object
Ex: "Student" class represents structure (Lit of Properties and Methods) of every student object.
We can create any no.of objects based on a class, using "new" keyword.
All teh Objects of a class, shares same set of properties & Methods of the class.
We can store the reference of the object in "reference variable", using which you can access the object
*/
var Student = /** @class */ (function () {
    function Student() {
    }
    Student.prototype.getResults = function () {
        if (this.marks >= 35) {
            return "Pass";
        }
        else {
            return "Fail";
        }
    };
    return Student;
}());
var s1 = new Student();
s1.studentId = 101;
s1.studentName = "Praveen Guptha N";
s1.marks = 80;
console.log(s1.studentId);
console.log(s1.studentName);
console.log(s1.marks);
console.log(s1.getResults());
