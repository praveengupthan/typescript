//clas start

class Employee {
    empId: any;
    empName: string;
    empExpYears: number;
    empRating: number;
    empDesignation: string;
    empFinalReview: string;

    //constructor start
    constructor(empId: any = 0, empName: string = 'none', empExpYears: number = 0, empRating: number = 0, empDesignation: string = 'none', empFinalReview: string = 'none') {

        this.empId = empId;
        this.empName = empName;
        this.empExpYears = empExpYears;
        this.empRating = empRating;
        this.empDesignation = empDesignation;
        this.empFinalReview = empFinalReview;
    }
    //constructor ends

    //get designation
    getDesignation() {
        if (this.empExpYears >= 10) {
            return 'Team Leader';
        } else {
            return 'Team Player';
        }
    }

    //get final review rating 
    getFinalReview() {
        if (this.empRating >= 5) {
            return 'He is Done Excellent Performance'
        } else (this.empRating < 5);
        {
            return 'He is Done Good Performance'
        }
    }
}

//class ends 

var E1 = new Employee("CORE001", "Praveen", 8, 4);
console.log(E1.empId);
console.log(E1.empName);
console.log(E1.empExpYears);
console.log(E1.getDesignation());
console.log(E1.getFinalReview());

var E2 = new Employee("CORE002", "Venkat", 12, 15);
console.log(E2.empId);
console.log(E2.empName);
console.log(E2.empExpYears);
console.log(E2.getDesignation());
console.log(E2.getFinalReview());

var E3 = new Employee("CORE003", "Paramesh", 5, 12);
console.log(E3.empId);
console.log(E3.empName);
console.log(E3.empExpYears);
console.log(E3.getDesignation());
console.log(E3.getFinalReview());


