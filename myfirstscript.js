//clas start
var Employee = /** @class */ (function () {
    //constructor start
    function Employee(empId, empName, empExpYears, empRating, empDesignation, empFinalReview) {
        if (empId === void 0) { empId = 0; }
        if (empName === void 0) { empName = 'none'; }
        if (empExpYears === void 0) { empExpYears = 0; }
        if (empRating === void 0) { empRating = 0; }
        if (empDesignation === void 0) { empDesignation = 'none'; }
        if (empFinalReview === void 0) { empFinalReview = 'none'; }
        this.empId = empId;
        this.empName = empName;
        this.empExpYears = empExpYears;
        this.empRating = empRating;
        this.empDesignation = empDesignation;
        this.empFinalReview = empFinalReview;
    }
    //constructor ends
    //get designation
    Employee.prototype.getDesignation = function () {
        if (this.empExpYears >= 10) {
            return 'Team Leader';
        }
        else {
            return 'Team Player';
        }
    };
    //get final review rating 
    Employee.prototype.getFinalReview = function () {
        if (this.empRating >= 5) {
            return 'He is Done Excellent Performance';
        }
        else
            (this.empRating < 5);
        {
            return 'He is Done Good Performance';
        }
    };
    return Employee;
}());
//class ends 
var E1 = new Employee("CORE001", "Praveen", 8, 4);
console.log(E1.empId);
console.log(E1.empName);
console.log(E1.empExpYears);
console.log(E1.getDesignation());
console.log(E1.getFinalReview());
var E2 = new Employee("CORE002", "Venkat", 12, 15);
console.log(E2.empId);
console.log(E2.empName);
console.log(E2.empExpYears);
console.log(E2.getDesignation());
console.log(E2.getFinalReview());
var E3 = new Employee("CORE003", "Paramesh", 5, 12);
console.log(E3.empId);
console.log(E3.empName);
console.log(E3.empExpYears);
console.log(E3.getDesignation());
console.log(E3.getFinalReview());
