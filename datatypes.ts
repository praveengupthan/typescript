/*
Data types specifies what type of value that can be  stored in a variable
Number: All Types of numbers (integer/floating Numbers EX: 10, 10.5)
string: Collection of characters in double quotes or single quotes ex:  "Hello World"
boolean: True or False
any: any type of value 
*/

let a: number = 25;
let b: string = "My Name is Praveen";
let c: boolean = true;
let d: any = "My Date of Birth is 14-08-1981";

console.log(a);
console.log(b);
console.log(c);
console.log(d);